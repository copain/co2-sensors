# Capteur de CO2 pour ESP2688

La version originale est développée sur
[Adafruit Feather HUZZAH with ESP8266](https://www.adafruit.com/product/2821).

Le capteur de CO2 est un [SparkFun SCD30](https://www.sensirion.com/scd30).

## Installation

La réalisation ne pose pas de difficultés. Il suffit d'alimenter
le capteur sur une broche 3,3V du nœud et de relier entre elles les broches
SDA et SCL du circuit et du capteur.

La compilation du projet s'effectue à l'aide de l'IDE Arduino. Elle nécessite
l'installation des bibliothèques de la carte
[ESP8266](https://github.com/esp8266/Arduino),
et utilise les librairies :

- [SparkFun_SCD30_Arduino_Library](https://github.com/sparkfun/SparkFun_SCD30_Arduino_Library)
- [ezTime](https://github.com/ropg/ezTime) pour la synchronisation
  avec un serveur de temps et la gestion des fuseaux horaires,
- et aussi les librairies ESP8266WiFi,ESP8266WebServer, ESP8266WebServerSecure
  fournies avec les librairies de la carte ESP8266.

La mémoire Flash est à configurer en mode "4MB (FS:2MB OTA:~1019KB)".
La librairie
[LittleFS Data Upload](https://github.com/lorol/arduino-esp32littlefs-plugin)
est nécessaire pour copier le contenu du dossier `data/` sur la mémoire flash
du nœud.

## Utilisation

Le nœud fonctionne en serveur HTTP, tant en point d'accès Wifi (IP: 192.168.4.1)
qu'en client Wifi (adresse donnée dans la console, ou sur la page web
accessible sur le point d'accès).

Lors du premier démarrage, seul le point d'accès sera disponible. Il faudra donc
s'y connecter (SSID par défaut : CO2pain, mot de passe : CO2chon2) puis
ouvrir un navigateur web sur [http://192.168.4.1](http://192.168.4.1) pour
renseigner les paramètres de connexion à un point d'accès Wifi.

Se connecter à un point d'accès Wifi est largement conseillé car ceci permet de
synchroniser la date et l'heure à un serveur NTP. Si ça n'est pas possible,
on peut régler la date et l'heure manuellement (la date et l'heure de
l'ordinateur qui se connecte est renseignée par défaut dans la page des réglages
de la date et l'heure).

On peut se connecter au nœud soit par son adresse externe (s'il est connecté
à un point d'accès) soit sur son adresse interne (si on y est connecté par
le point d'accès qu'il fournit). On peut se connecter en HTTP comme en HTTPS.
Toutefois, les certificats
utilisés pour le HTTPS sont auto-signés, c'est-à-dire que le navigateur
commencera par signaler une alerte de sécurité à laquelle il faudra
passer outre. À noter que le mode HTTPS est extrêmement lent (il oblige le
nœud à utiliser ses maigres ressources pour crypter toutes les communications).

### Génération de certificats

Voir [https://github.com/esp8266/Arduino/blob/master/doc/esp8266wifi/bearssl-server-secure-class.rst]()

Il est important de **générer ses propres certificats**. Ceux fournis en exemple
sont par définition éventés donc faillibles.

```sh
openssl req -x509 -nodes -newkey rsa:2048 -keyout key.pem -out cert.pem -days 4096
```

Il faut ensuite copier le contenu des deux fichiers générés dans `certs.h` :

- `cert.pem` dans *serverCert[]*,
- `key.pem` dans *serverKey[]*.

#### Remarque

Les certificats fournis dans l'exemple sont des certificats de 512 octets
(changer la valeur après rsa: dans la ligne de commande pour générer les
certificats). Ceci accélère quelque peu les échanges. En revanche c'est moins
sécurisé.

### MQTT

Une fois connecté à un point d'accès, si celui-ci dispose d'un broker MQTT, le noeud tentera de publier les mesures du capteur. Le format du topic est `IdNoeud/ReferenceNoeud/ReferenceCapteur/TypeMesure`.

## À noter

La page permettant la visualisation des courbes de mesures utilise
[moChart.js](https://github.com/bigbird231/mobile-chart). Son code
a été repris pour le rendre plus condensé et en supprimer les fonctions
non utilisées afin de l'alléger autant que possible.
