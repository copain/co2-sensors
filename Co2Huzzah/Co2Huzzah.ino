#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266mDNS.h>
#include <Wire.h>
#include <SPI.h>
#include <LittleFS.h> /* doc: https://github.com/espressif/arduino-esp32/tree/master/libraries/LittleFS */
#include <ezTime.h>
#include "SparkFun_SCD30_Arduino_Library.h"

/* If defined, HTTPS otherwise HTTP. */
#include <ESP8266WebServerSecure.h>
#include <umm_malloc/umm_malloc.h>
#include <umm_malloc/umm_heap_select.h>
#include "certs.h"
#include <ESP8266WebServer.h>

/* MQTT Lib */
#include <PubSubClient.h>

/*  ==================== Variables ==================== */
/* If defined, then mode is 1 measure every 15 min. written in a file.
   Setup is not done. */
/*#define DEMO_MODE*/

/* ---------- Default values ---------- */
#define SERIAL_BAUDRATE 115200
#define MEASUREMENT_INTERVAL 60  /* seconds */
#define NTP_UPDATE_INTERVAL 1800 /* 30 min = 1800s. */
#define ALTITUDE 352             /* Clermont les Carmes */
#define DEFAULT_SSID "irstea_invite"
#define DEFAULT_HOST "CO2opain"
#define DEFAULT_AP_PASSWD "CO2chon2"
#define DEFAULT_MQTT_BROKER "10.63.65.95"

/* ------------------------------------------------- */
/*     Keep some usual Strings static and shared     */
/* ------------------------------------------------- */
static String MSG[7] = {
    "[ERROR] ",
    "[WARN] ",
    "[INFO] ",
    "application/octet-stream",
    "application/json",
    "text/html",
    "HTTP1.1 required",
};

#define ERR 0
#define WARN 1
#define INFO 2
#define MIME_STREAM 3
#define MIME_JSON 4
#define MIME_HTML 5
#define SRV_MSG_HTTP 6

/* ------------------------------------------------- */

static char ssid[48];
static char password[72];
static char host[48];
static char ap_password[72]; /* Max 47 char */
static int ntpUpdateInterval = NTP_UPDATE_INTERVAL;

static bool writeData = false;

static String ntpServerName = "fr.pool.ntp.org";
static String timezone = "Europe/Brussels";
const char *timeformat = "Y-m-d~TH:i:s;T";

/* ---------- filesystem ---------- */
const char *fsName = "LittleFS";
FS *fileSystem = &LittleFS;
LittleFSConfig fileSystemConfig = LittleFSConfig();
static bool fsOK;
String unsupportedFiles = String();

File uploadFile;

static const char TEXT_PLAIN[] PROGMEM = "text/plain";
static const char FS_INIT_ERROR[] PROGMEM = "FS INIT ERROR";
static const char FILE_NOT_FOUND[] PROGMEM = "FileNotFound";

/* ---------- NTP ---------- */
static Timezone tz;
static bool tzOK = false;

/* ---------- webserver ---------- */
static bool wifiClientOK = false;
BearSSL::ESP8266WebServerSecure HTTPSserver(443);
BearSSL::ServerSessions serverCache(5);
ESP8266WebServer HTTPserver(80);

/* ---------- sensor ---------- */
#define SENSOR_JSON_SIZE 72 /* Actually exactly 68 */
SCD30 airSensor;
static bool sensorOK = false;
static float co2 = 0.0, temperature = 0.0, humidity = 0.0;
static String sensorJson = String();
static int interval = MEASUREMENT_INTERVAL;
static int altitude = ALTITUDE;

/* ---------- global variablres ---------- */
#define BUF_SIZE 256
#define BLUE_LED 2
#define RED_LED 0
static String buf = String(); /* A String reserved once for many local uses */
static String datafile = String();
static int i, j;
static unsigned long l = 0l;
static char cbuf[BUF_SIZE];

static bool LED_STATUS = false;

/* ---------- MQTT ------------------------*/
static char mqtt_broker[16];
const char *topic_co2 = "N4/ESP8266/SDC30/CO2";
const char *topic_temp = "N4/ESP8266/SDC30/airTemperature";
const char *topic_hum = "N4/ESP8266/SDC30/airHumidity";
static int mqtt_port = 1883;
const char *client_id = "CO2Pain";

WiFiClient espClient;
PubSubClient client(espClient);

/*  ==================== Fonctions ==================== */

/*----------- MQTT ---------- */

void publishMQTT() {
  for (int i = 0; (i < 5) && (!client.connected()); i++) {
    client.connect(client_id);
    delay(2000);
  }
  if (!client.connected()) return;
  dtostrf(co2, 5, 0, cbuf);
  client.publish(topic_co2, cbuf);
  dtostrf(temperature, 6, 2, cbuf);
  client.publish(topic_temp, cbuf);
  dtostrf(humidity, 6, 2, cbuf);
  client.publish(topic_hum, cbuf);
}

/* ---------- HTTP ---------- */

template <typename ServerType>
void replyOK(esp8266webserver::ESP8266WebServerTemplate<ServerType> *server)
{
  (*server).send(200, FPSTR(TEXT_PLAIN), "");
}

template <typename ServerType>
void replyOKWithMsg(esp8266webserver::ESP8266WebServerTemplate<ServerType> *server, String msg)
{
  (*server).send(200, FPSTR(TEXT_PLAIN), msg);
}

template <typename ServerType>
void replyNotFound(esp8266webserver::ESP8266WebServerTemplate<ServerType> *server, String msg)
{
  (*server).send(404, FPSTR(TEXT_PLAIN), msg);
}

template <typename ServerType>
void replyBadRequest(esp8266webserver::ESP8266WebServerTemplate<ServerType> *server, String msg)
{
  (*server).send(400, FPSTR(TEXT_PLAIN), msg + "\n");
}

template <typename ServerType>
void replyServerError(esp8266webserver::ESP8266WebServerTemplate<ServerType> *server, String msg)
{
  (*server).send(500, FPSTR(TEXT_PLAIN), msg + "\n");
}

/* Reads the given file from the filesystem and stream it back to the client */
template <typename ServerType>
bool handleFileRead(esp8266webserver::ESP8266WebServerTemplate<ServerType> *server, String path)
{
  if (!fsOK)
  {
    replyServerError(server, FPSTR(FS_INIT_ERROR));
    return true;
  }
  if (path.startsWith("/wifiSetup.cfg") || path.startsWith("/apSetup.cfg"))
  {
    replyNotFound(server, "File not found.");
    return true;
  }

  if (path.endsWith("/"))
    path += "index.htm";
  if (!fileSystem->exists(path))
    path += "l";
  if (!fileSystem->exists(path))
    return false; // Re-try with .htm*l*

  if ((*server).hasArg("download"))
    buf = MSG[MIME_STREAM];
  else
    buf = mime::getContentType(path);

  File file = fileSystem->open(path, "r");
  if ((*server).streamFile(file, buf) != file.size())
  {
    // DBG_OUTPUT_PORT.println("Sent less data than expected!");
  }
  file.close();
  return true;
}
bool HTTPhandleFileRead(String path) { return handleFileRead(&HTTPserver, path); }
bool HTTPShandleFileRead(String path) { return handleFileRead(&HTTPSserver, path); }

/* The "Not Found" handler catches all URI not explicitly declared in code
   First try to find and return the requested file from the filesystem,
   and if it fails, return a 404 page with debug information */
template <typename ServerType>
void handleNotFound(esp8266webserver::ESP8266WebServerTemplate<ServerType> *server)
{
  if (!fsOK)
    return replyServerError(server, FPSTR(FS_INIT_ERROR));

  buf = ESP8266WebServer::urlDecode((*server).uri()); // required to read paths with blanks
  if (handleFileRead(server, buf))
    return;

  // Dump debug data
  buf = F("Error: File not found\n\nURI: ") + buf;
  buf += F("\nMethod: ");
  buf += ((*server).method() == HTTP_GET) ? "GET" : "POST";
  buf += F("\nArguments: ");
  buf += (*server).args();
  buf += '\n';
  for (i = 0; i < (*server).args(); i++)
  {
    buf += F(" NAME:");
    buf += (*server).argName(i);
    buf += F("\n VALUE:");
    buf += (*server).arg(i);
    buf += '\n';
  }
  buf += "path=";
  buf += (*server).arg("path");
  buf += '\n';

  return replyNotFound(server, buf);
}
void HTTPhandleNotFound() { return handleNotFound(&HTTPserver); }
void HTTPShandleNotFound() { return handleNotFound(&HTTPSserver); }

/* Returns the list of files in the directory specified by the "dir" query string parameter.
   Also demonstrates the use of chunked responses. */
template <typename ServerType>
void handleFileList(esp8266webserver::ESP8266WebServerTemplate<ServerType> *server)
{
  if (!fsOK)
    return replyServerError(server, FPSTR(FS_INIT_ERROR));

  if (!fileSystem->exists("/data"))
  {
    (*server).send(200, MSG[MIME_JSON], "[]");
    return;
  }

  // use HTTP/1.1 Chunked response to avoid building a huge temporary string
  if (!(*server).chunkedResponseModeStart(200, MSG[MIME_JSON]))
  {
    (*server).send(505, MSG[MIME_HTML], MSG[SRV_MSG_HTTP]);
    return;
  }

  Dir dir = fileSystem->openDir("/data");
  buf = ""; // Will use the same string for every line
  while (dir.next())
  {
    if (buf.length())
    { // send string from previous iteration as an HTTP chunk
      (*server).sendContent(buf);
      buf = ',';
    }
    else
      buf = '[';

    if (!dir.isDirectory())
    {
      buf += F("{\"size\":\"");
      buf += dir.fileSize();
    }

    buf += F("\",\"name\":\"");
    // Always return names without leading "/"
    if (dir.fileName()[0] == '/')
      buf += &(dir.fileName()[1]);
    else
      buf += dir.fileName();

    buf += "\"}";
  }

  buf += "]"; // send last string
  (*server).sendContent(buf);
  (*server).chunkedResponseFinalize();
}
void HTTPhandleFileList() { return handleFileList(&HTTPserver); }
void HTTPShandleFileList() { return handleFileList(&HTTPSserver); }

template <typename ServerType>
void handleWifiScan(esp8266webserver::ESP8266WebServerTemplate<ServerType> *server)
{
  int n = WiFi.scanNetworks();
  if (!(*server).chunkedResponseModeStart(200, MSG[MIME_JSON]))
  {
    (*server).send(505, MSG[MIME_HTML], MSG[SRV_MSG_HTTP]);
    return;
  }
#ifdef DEMO_MODE
  (*server).sendContent(F("[\"* AP_1 (-71)\",\"  AP_2 (-59)\",\"* AP_3 (-75)\"]"));
#else
  buf = '[';
  for (i = 0; i < n; buf = "")
  {
    buf += "\"";
    buf += ((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
    buf += " " + WiFi.SSID(i) + " (" + WiFi.RSSI(i) + ")\"";
    if (++i == n)
      buf += "]";
    else
      buf += ",";
    (*server).sendContent(buf);
  }
#endif
  (*server).chunkedResponseFinalize();
}
void HTTPhandleWifiScan() { return handleWifiScan(&HTTPserver); }
void HTTPShandleWifiScan() { return handleWifiScan(&HTTPSserver); }

bool wifiConnect(char *ssId, char *pw)
{
  WiFi.begin(ssId, pw);
  delay(500);
  i = 0; /* 60 loops ~30s. */
  while ((WiFi.status() != WL_CONNECTED) && (i++ < 60))
    delay(500);
  if (WiFi.status() == WL_CONNECTED)
  {
    Serial.print(MSG[INFO]);
    Serial.print(F("Wifi connected to "));
    Serial.print(ssId);
    Serial.print(F(". IP address: "));
    Serial.println(WiFi.localIP());
    return true;
  }
  Serial.print(MSG[WARN]);
  Serial.print(F("Cannot connect to "));
  Serial.print(ssId);
  Serial.println(".");
  return false;
}

/* Initilizes Wifi. Uses global variables ssid, assword, host, ap_password.
 * Returns true if connected to an AP. */
bool initWifi()
{
  WiFi.mode(WIFI_AP_STA); /* Client AND AP */
  delay(100);
  WiFi.softAP(host, ap_password);
  delay(100);
  client.setServer(mqtt_broker, mqtt_port);
  delay(100);
  return wifiConnect(ssid, password);
}

template <typename ServerType>
void handleWifiSet(esp8266webserver::ESP8266WebServerTemplate<ServerType> *server) {
  bool modif = false, cnx, donotsave = true;
#ifndef DEMO_MODE
  buf = (*server).arg("broker");
  if (buf.length() > 0) {
    modif = true;
    buf.toCharArray(mqtt_broker, sizeof(mqtt_broker));
    buf = (*server).arg("port");
    mqtt_port = buf.toInt();
  }
  buf = (*server).arg("ssid");
  if (buf.length() > 0) {
    modif = true;
    buf.toCharArray(ssid, sizeof(ssid));
    buf = (*server).arg("pass");
    buf.toCharArray(password, sizeof(password));
    donotsave = (*server).hasArg("save");
  }
  if (!modif) return;

  cnx = initWifi();
  if (cnx) {
    if ((fsOK) && (!donotsave)) {
      Serial.print(MSG[INFO]);
      Serial.println(F("Saving Wifi configuration."));
      File file = fileSystem->open("/wifiSetup.cfg", "w");
      file.println(ssid);
      file.println(password);
      file.println(mqtt_broker);
      file.println(mqtt_port);
      delay(10);
      file.close();
    }
  } else {
    Serial.print(MSG[WARN]);
    Serial.println(F("Resetting."));
    ESP.reset();
  }
#endif
  handleFileRead(server, "/");
}
void HTTPhandleWifiSet() { return handleWifiSet(&HTTPserver); }
void HTTPShandleWifiSet() { return handleWifiSet(&HTTPSserver); }

template <typename ServerType>
void handleAP(esp8266webserver::ESP8266WebServerTemplate<ServerType> *server)
{
#ifdef DEMO_MODE
  buf = "{\"ap\":\"";
  buf += host;
  buf += "\",\"pw\":\"demo\"}";
#else
  buf = "{\"ap\":\"";
  buf += host;
  buf += "\",\"pw\":\"";
  buf += ap_password;
  buf += "\"}";
#endif
  (*server).send(200, MSG[MIME_JSON], buf);
}
void HTTPhandleAP() { return handleAP(&HTTPserver); }
void HTTPShandleAP() { return handleAP(&HTTPSserver); }

template <typename ServerType>
void handleMQTTbroker(esp8266webserver::ESP8266WebServerTemplate<ServerType> *server) {
  buf = "{\"broker\":\"";
  buf += mqtt_broker;
  buf += "\",\"port\":\"";
  buf += mqtt_port;
  buf += "\"}";
  (*server).send(200, MSG[MIME_JSON], buf);
}

void HTTPhandleMQTTbroker() { return handleMQTTbroker(&HTTPserver); }
void HTTPShandleMQTTbroker() { return handleMQTTbroker(&HTTPSserver); }

template <typename ServerType>
void handleAPSet(esp8266webserver::ESP8266WebServerTemplate<ServerType> *server)
{
  bool donotsave = true;
#ifndef DEMO_MODE
  buf = (*server).arg("pass");
  if (buf.length() < 8)
  {
    replyBadRequest(server, F("Password too short (8 char. min)"));
    return;
  }
  if (buf.length() > 0)
  {
    buf.toCharArray(ap_password, sizeof(ap_password));
    buf = (*server).arg("ssid");
    buf.toCharArray(host, sizeof(host));
    donotsave = (*server).hasArg("save");
  }
  else
    return;
  initWifi();
  if ((fsOK) && (!donotsave))
  {
    Serial.print(MSG[INFO]);
    Serial.println(F("Saving AP configuration."));
    File file = fileSystem->open("/apSetup.cfg", "w");
    file.println(host);
    file.println(ap_password);
    delay(10);
    file.close();
  }
#endif
  handleFileRead(server, "/");
}


void HTTPhandleAPSet() { return handleAPSet(&HTTPserver); }
void HTTPShandleAPSet() { return handleAPSet(&HTTPSserver); }

template <typename ServerType>
void handleSensorSet(esp8266webserver::ESP8266WebServerTemplate<ServerType> *server)
{
  bool donotsave = (*server).hasArg("save");
  bool w = (*server).hasArg("write");
#ifndef DEMO_MODE
  buf = (*server).arg("ftxt");
  i = buf.toInt();
  if ((i > 0) && (i != interval))
  {
    interval = i;
    airSensor.setMeasurementInterval(interval);
  }
  buf = (*server).arg("alt");
  j = buf.toInt();
  if ((j >= 0) && (j != altitude))
  {
    altitude = j;
    airSensor.setAltitudeCompensation(altitude);
  }
  if ((w) && (!writeData))
    datafile = "data/" + tz.dateTime("YmdHi") + ".csv";
  writeData = w;
  if ((fsOK) && (!donotsave))
  {
    Serial.print(MSG[INFO]);
    Serial.println(F("Saving Sensor configuration."));
    File file = fileSystem->open("/sensorSetup.cfg", "w");
    file.println(interval);
    file.println(altitude);
    file.println((writeData) ? "y" : "n");
    delay(10);
    file.close();
  }
#endif
  handleFileRead(server, "/");
}
void HTTPhandleSensorSet() { return handleSensorSet(&HTTPserver); }
void HTTPShandleSensorSet() { return handleSensorSet(&HTTPSserver); }

template <typename ServerType>
void handleStatus(esp8266webserver::ESP8266WebServerTemplate<ServerType> *server)
{
  IPAddress ip = WiFi.softAPIP();
  FSInfo fs_info;
  if (!(*server).chunkedResponseModeStart(200, MSG[MIME_JSON]))
  {
    (*server).send(505, MSG[MIME_HTML], MSG[SRV_MSG_HTTP]);
    return;
  }

  buf = F("{\"ap\":\"");
  buf += host;
  buf += F("\",\"aip\":\"");
  for (i = 0; i < 4; buf += ((i == 4) ? "" : "."))
    buf += ip[i++];
  buf += "\",";
  (*server).sendContent(buf);

  buf = F("\"ssid\":\"");
  if (WiFi.status() == WL_CONNECTED)
  {
    ip = WiFi.localIP();
    buf += ssid;
    buf += "\",\"ip\":\"";
    for (i = 0; i < 4; buf += ((i == 4) ? "" : "."))
      buf += ip[i++];
  }
  else
    buf += "\",\"ip\":\"";
  buf += "\",";
  (*server).sendContent(buf);

  buf = F("\"file\":\"");
  if ((fsOK) && (writeData))
    buf += datafile;
  (*server).sendContent(buf);
  buf = F("\",\"freq\":");
  buf += (sensorOK) ? interval : -1;
  buf += F(",\"alt\":");
  buf += (sensorOK) ? altitude : -1;
  buf += ",";
  (*server).sendContent(buf);

  buf = F("\"du\":");
  if (fsOK)
  {
    fileSystem->info(fs_info);
    buf += fs_info.usedBytes;
    buf += F(",\"ds\":");
    buf += fs_info.totalBytes;
  }
  else
    buf += F("-1,\"ds\":-1");
  buf += "}";
  (*server).sendContent(buf);
  (*server).chunkedResponseFinalize();
}
void HTTPhandleStatus() { return handleStatus(&HTTPserver); }
void HTTPShandleStatus() { return handleStatus(&HTTPSserver); }

template <typename ServerType>
void handleSetup(esp8266webserver::ESP8266WebServerTemplate<ServerType> *server)
{
  buf = F("{\"freq\":");
  buf += (sensorOK) ? interval : -1;
  buf += F(",\"alt\":");
  buf += (sensorOK) ? altitude : -1;
  buf += F(",\"write\":");
  buf += (writeData) ? "true" : "false";
  buf += "}";
  (*server).send(200, MSG[MIME_JSON], buf);
}
void HTTPhandleSetup() { return handleSetup(&HTTPserver); }
void HTTPShandleSetup() { return handleSetup(&HTTPSserver); }

/* ---------- Date and time ---------- */
template <typename ServerType>
void handleDateTime(esp8266webserver::ESP8266WebServerTemplate<ServerType> *server)
{
  int h, m, d, mo, y;
#ifndef DEMO_MODE
  buf = (*server).arg("H");
  h = buf.toInt();
  buf = (*server).arg("M");
  m = buf.toInt();
  buf = (*server).arg("d");
  d = buf.toInt();
  buf = (*server).arg("mo");
  mo = buf.toInt();
  buf = (*server).arg("y");
  y = buf.toInt();
  setTime(h, m, 30, d, mo, y);
#endif
  handleFileRead(server, "/");
}
void HTTPhandleDateTime() { return handleDateTime(&HTTPserver); }
void HTTPShandleDateTime() { return handleDateTime(&HTTPSserver); }

template <typename ServerType>
void handleNTP(esp8266webserver::ESP8266WebServerTemplate<ServerType> *server)
{
  bool donotsave = (*server).hasArg("save");
  bool chg = false;

#ifndef DEMO_MODE
  buf = (*server).arg("ntp");
  if (chg = (ntpServerName != buf))
  {
    ntpServerName = buf;
    setServer(ntpServerName);
  }

  buf = (*server).arg("tz");
  if (timezone != buf)
    if (!tz.setLocation(buf))
    {
      Serial.print(MSG[WARN]);
      Serial.print(F("Timezone setting failed -> "));
      Serial.println(buf);
    }
    else
    {
      chg = true;
      timezone = buf;
    }

  buf = (*server).arg("req");
  i = buf.toInt();
  if (ntpUpdateInterval != i)
  {
    chg = true;
    setInterval(ntpUpdateInterval);
  }

  if ((fsOK) && (chg) && (!donotsave))
  {
    Serial.print(MSG[INFO]);
    Serial.println(F("Saving NTP configuration."));
    File file = fileSystem->open("/NTPSetup.cfg", "w");
    file.println(ntpServerName);
    file.println(timezone);
    file.println(ntpUpdateInterval);
    delay(10);
    file.close();
  }
#endif
  handleFileRead(server, "/");
}
void HTTPhandleNTP() { return handleNTP(&HTTPserver); }
void HTTPShandleNTP() { return handleNTP(&HTTPSserver); }

template <typename ServerType>
void handleDateTimeZone(esp8266webserver::ESP8266WebServerTemplate<ServerType> *server)
{
  if (!(*server).chunkedResponseModeStart(200, MSG[MIME_JSON]))
  {
    (*server).send(505, MSG[MIME_HTML], MSG[SRV_MSG_HTTP]);
    return;
  }
  (*server).sendContent(F("{\"tz\":\""));
  (*server).sendContent(timezone);
  (*server).sendContent(F("\",\"ntp\":\""));
  (*server).sendContent(ntpServerName);
  (*server).sendContent(F("\",\"freq\":\""));
  buf = "";
  buf += ntpUpdateInterval;
  if (buf == "")
    buf = "1800";
  (*server).sendContent(buf);
  (*server).sendContent(F("\",\"sync\":"));
  buf = (timeStatus() == timeSet) ? "true" : "false";
  buf += "}";
  (*server).sendContent(buf);
  (*server).chunkedResponseFinalize();
}
void HTTPhandleDateTimeZone() { return handleDateTimeZone(&HTTPserver); }
void HTTPShandleDateTimeZone() { return handleDateTimeZone(&HTTPSserver); }

/* ---------- CO2 sensor ---------- */
void HTTPhandleSensor()
{ /* RQ : longueur du message : 68 octets. */
  HTTPserver.send(200, MSG[MIME_JSON], sensorJson);
}
void HTTPShandleSensor() { HTTPSserver.send(200, MSG[MIME_JSON], sensorJson); }

/* Fills co2, temperature and humidity values if new values
 * are available from sensor. Returns true if new values read.
 */
bool getSensorData()
{
  if (!airSensor.dataAvailable())
    return false;
  co2 = airSensor.getCO2();
  temperature = airSensor.getTemperature();
  humidity = airSensor.getHumidity();
  /* sensorJson */
  sensorJson = "{\"d\":\"" + tz.dateTime("d/m/Y");
  sensorJson += "\",\"t\":\"" + tz.dateTime("H:i");
  sensorJson += "\",\"co2\":";
  sensorJson += co2;
  sensorJson += ",\"temp\":" + String(temperature, 2);
  sensorJson += ",\"hum\":" + String(humidity, 2) + "}";
  return true;
}

template <typename ServerType>
void handleFileManage(esp8266webserver::ESP8266WebServerTemplate<ServerType> *server)
{
  String ch;

  if (!fsOK)
    return replyServerError(server, FPSTR(FS_INIT_ERROR));
  if (!(*server).hasArg("file"))
    return replyBadRequest(server, F("No file selected"));
  buf = "/data/" + (*server).arg("file");
  if ((*server).arg("todo").startsWith("get"))
  {
    /* Already done by javascript code */
    return replyOK(server);
  }
  else
  {
    if ((*server).arg("todo").startsWith("del"))
    { /* to be sure */
      Serial.print(MSG[INFO]);
#ifdef DEMO_MODE
      Serial.print(F("(demo:) Not deleting file "));
      Serial.println(buf);
#else
      Serial.print(F("Deleting file "));
      Serial.println(buf);
      if (!LittleFS.remove(buf))
      {
        Serial.print(MSG[ERR]);
        Serial.println("Delete failed");
      }
#endif
    }
    return replyOK(server);
  }
  handleFileRead(server, "/download.html");
}
void HTTPhandleFileManage() { return handleFileManage(&HTTPserver); }
void HTTPShandleFileManage() { return handleFileManage(&HTTPSserver); }

/* ---------- Tools ---------- */
void readLineInFile(File &f, char *c, int len)
{
  buf = f.readStringUntil('\n');
  for (i = 0; ((int)buf.charAt(i)) > 31; i++)
    ;
  if (i < buf.length())
    buf.remove(i);
  buf.toCharArray(c, len);
}

/*  ==================== Main ==================== */
void setup()
{
  /* --- Initialize default values. --- */
  buf.reserve(BUF_SIZE);
  buf = DEFAULT_SSID;
  buf.toCharArray(ssid, sizeof(ssid));
  buf = DEFAULT_HOST;
  buf.toCharArray(host, sizeof(host));
  buf = DEFAULT_AP_PASSWD;
  buf.toCharArray(ap_password, sizeof(ap_password));
  buf = DEFAULT_MQTT_BROKER;
  buf.toCharArray(mqtt_broker, sizeof(mqtt_broker));
  buf = "";
  buf.toCharArray(password, sizeof(password));

  datafile.reserve(16); /* YYYYmmddHHmm = 12, prefixed with "-NS" if time not set. */
  sensorJson.reserve(SENSOR_JSON_SIZE);
  sensorJson = "{\"d\":null,\"t\":null,\"co2\":null,\"temp\":null,\"hum\":null}";
  WiFi.persistent(false);
  Serial.begin(SERIAL_BAUDRATE);
  delay(50);
  Wire.begin();
  delay(50);
  pinMode(BLUE_LED, OUTPUT);
  pinMode(RED_LED, OUTPUT);
  delay(1000);

  fileSystemConfig.setAutoFormat(false);
  fileSystem->setConfig(fileSystemConfig);
  fsOK = fileSystem->begin();

  if (!fsOK) {
    Serial.print(MSG[WARN]);
    Serial.println(F("Filesystem init failed !"));
  } else {
    Serial.print(MSG[INFO]);
    Serial.println(F("Reading configuration files."));
    File file = fileSystem->open("/wifiSetup.cfg", "r");
    if (file) {
      readLineInFile(file, ssid, sizeof(ssid));
      readLineInFile(file, password, sizeof(password));
      readLineInFile(file, mqtt_broker, sizeof(mqtt_broker));
      readLineInFile(file, cbuf, sizeof(cbuf));
      buf = cbuf;
      mqtt_port = buf.toInt();
      file.close();
    } else {
      Serial.print(MSG[WARN]);
      Serial.println(F("No wifi setup file found."));
    }

#ifndef DEMO_MODE
    file = fileSystem->open("/apSetup.cfg", "r");
    if (file)
    {
      readLineInFile(file, host, sizeof(host));
      readLineInFile(file, ap_password, sizeof(ap_password));
      file.close();
    }
    else
    {
      Serial.print(MSG[WARN]);
      Serial.println(F("No AP setup file found."));
    }
#endif

    file = fileSystem->open("/sensorSetup.cfg", "r");
    if (file)
    {
      readLineInFile(file, cbuf, sizeof(cbuf));
      buf = cbuf;
      interval = buf.toInt();
      readLineInFile(file, cbuf, sizeof(cbuf));
      buf = cbuf;
      altitude = buf.toInt();
      readLineInFile(file, cbuf, sizeof(cbuf));
      writeData = (cbuf[0] == 'y');
      file.close();
    }
    else
    {
      Serial.print(MSG[WARN]);
      Serial.println(F("No sensor setup file found."));
    }
#ifdef DEMO_MODE
    writeData = true;
    altitude = ALTITUDE;
    interval = 900;
#endif

    file = fileSystem->open("/NTPSetup.cfg", "r");
    if (file)
    {
      readLineInFile(file, cbuf, sizeof(cbuf));
      ntpServerName = cbuf;
      readLineInFile(file, cbuf, sizeof(cbuf));
      timezone = cbuf;
      readLineInFile(file, cbuf, sizeof(cbuf));
      buf = cbuf;
      ntpUpdateInterval = buf.toInt();
      file.close();
    }
    else
    {
      Serial.print(MSG[WARN]);
      Serial.println(F("No NTP setup file found."));
    }
  }
  digitalWrite(RED_LED, HIGH); /* HIGH value turns it off */

  /* WiFi.mode(WIFI_STA); */ /* Client only */
  WiFi.mode(WIFI_AP_STA);    /* Client AND AP */
  delay(100);
  WiFi.softAP(host, ap_password);
  delay(100);
  wifiConnect(ssid, password);

  /* MQTT init */
  client.setServer(mqtt_broker, mqtt_port);

  Serial.print(MSG[INFO]);
  Serial.print(F("AP IP address: "));
  Serial.println(WiFi.softAPIP());

  if (MDNS.begin(host)) {
    MDNS.addService("http", "tcp", 80);
    MDNS.addService("https", "tcp", 443);
  } else {
    Serial.print(MSG[WARN]);
    Serial.println(F("Error setting up MDNS responder!"));
  }

  /* HTTPS server */
  HTTPSserver.getServer().setRSACert(new BearSSL::X509List(serverCert), new BearSSL::PrivateKey(serverKey));
  // Cache SSL sessions to accelerate the TLS handshake.
  HTTPSserver.getServer().setCache(&serverCache);

  /*server.on("/", HTTP_GET, handleHome);*/ /* Done in handleNotFound */
  HTTPserver.on("/sensor", HTTP_GET, HTTPhandleSensor);
  HTTPserver.on("/listfiles", HTTP_GET, HTTPhandleFileList);
  HTTPserver.on("/scan", HTTP_GET, HTTPhandleWifiScan);
  HTTPserver.on("/mqttbroker", HTTP_GET, HTTPhandleMQTTbroker);
  HTTPserver.on("/ap", HTTP_GET, HTTPhandleAP);
  HTTPserver.on("/status", HTTP_GET, HTTPhandleStatus);
  HTTPserver.on("/dtz", HTTP_GET, HTTPhandleDateTimeZone);
  HTTPserver.on("/sensorsetup", HTTP_GET, HTTPhandleSetup);
  HTTPserver.on("/WifiSetting", HTTP_GET, HTTPhandleWifiSet);
  HTTPserver.on("/APSetting", HTTP_GET, HTTPhandleAPSet);
  HTTPserver.on("/SensorSetting", HTTP_GET, HTTPhandleSensorSet);
  HTTPserver.on("/FileManage", HTTP_GET, HTTPhandleFileManage);
  HTTPserver.on("/DateTime", HTTP_GET, HTTPhandleDateTime);
  HTTPserver.on("/NTPSet", HTTP_GET, HTTPhandleNTP);
  HTTPserver.onNotFound(HTTPhandleNotFound);
  HTTPserver.begin();

  HTTPSserver.on("/sensor", HTTP_GET, HTTPShandleSensor);
  HTTPSserver.on("/listfiles", HTTP_GET, HTTPShandleFileList);
  HTTPSserver.on("/scan", HTTP_GET, HTTPShandleWifiScan);
  HTTPSserver.on("/mqttbroker", HTTP_GET, HTTPShandleMQTTbroker);
  HTTPSserver.on("/ap", HTTP_GET, HTTPShandleAP);
  HTTPSserver.on("/status", HTTP_GET, HTTPShandleStatus);
  HTTPSserver.on("/dtz", HTTP_GET, HTTPShandleDateTimeZone);
  HTTPSserver.on("/sensorsetup", HTTP_GET, HTTPShandleSetup);
  HTTPSserver.on("/WifiSetting", HTTP_GET, HTTPShandleWifiSet);
  HTTPSserver.on("/APSetting", HTTP_GET, HTTPShandleAPSet);
  HTTPSserver.on("/SensorSetting", HTTP_GET, HTTPShandleSensorSet);
  HTTPSserver.on("/FileManage", HTTP_GET, HTTPShandleFileManage);
  HTTPSserver.on("/DateTime", HTTP_GET, HTTPShandleDateTime);
  HTTPSserver.on("/NTPSet", HTTP_GET, HTTPShandleNTP);
  HTTPSserver.onNotFound(HTTPShandleNotFound);
  HTTPSserver.begin();

  /* NTP */
  setDebug(NONE);
  setServer(ntpServerName);
  waitForSync(60);  /* Waits NTP sync for max. 60 seconds. */
  setInterval(120); /* Try every 2 minutes until success */
  if (!tz.setLocation(timezone)) {
    Serial.print(MSG[WARN]);
    Serial.println(F("Timezone setting failed."));
    /* tz = UTC; */
  }
  digitalWrite(BLUE_LED, HIGH); /* HIGH value turns it off */

  delay(1000);
  i = 0;
  while ((!(sensorOK = airSensor.begin())) && (i++ < 6)) {
    Serial.print(MSG[ERR]);
    Serial.println(F("Air sensor not detected. Please check wiring. Waiting 10s..."));
    delay(10000);
  }
  if (sensorOK) {
    airSensor.setMeasurementInterval(interval);
    airSensor.setAltitudeCompensation(altitude);
  }
  delay(200);

  /* Set datafile name */
  tzOK = (timeStatus() == timeSet);
  if (tzOK) {
    setInterval(ntpUpdateInterval);
    datafile = "data/" + tz.dateTime("YmdHi") + ".csv";
    Serial.print(MSG[INFO]);
    Serial.print(F("NTP date and time set : "));
    Serial.println(tz.dateTime(timeformat));
  } else {
    Serial.print(MSG[WARN]);
    Serial.println(F("NTP date and time not set."));
    if (fsOK) { /* If !(fsOK), we won't save any data. */
      Dir dir = fileSystem->openDir("/data");
      j = 0;
      while (dir.next()) {
        if (!dir.isDirectory()) {
          // Always return names without leading "/"
          if (dir.fileName()[0] == '/')
            buf = &(dir.fileName()[1]);
          else
            buf = dir.fileName();
          i = buf.substring(0, 11).toInt();
          if (i > j)
            j = i;
        }
      }
      if (j > 0)
        datafile = "data/" + String(j) + "-NS.csv";
      else
        datafile = F("data/197001010000-NS.csv");
    } else {
      datafile = "";
    }
  }
  if ((datafile.length() > 0) && (writeData)) {
    Serial.print(MSG[INFO]);
    Serial.print(F("Writing data into "));
    Serial.println(datafile);
  }

  /* --- MQTT --- */
  /*publishMQTT();*/ /* La première mesure est vide */
}

void loop()
{
  File file;

  /* --- NTP sync --- */
  events();
  /* NTP time just set */
  if ((!tzOK) && (timeStatus() == timeSet))
  {
    setInterval(NTP_UPDATE_INTERVAL);
    Serial.print(MSG[INFO]);
    Serial.print(F("NTP date and time set : "));
    Serial.println(tz.dateTime(timeformat));
    tzOK = true;
    if (fsOK)
    { /* Have to rename filename */
      buf = "data/" + tz.dateTime("YmdHi") + ".csv";
      if ((writeData) && (datafile.length() > 0))
      {
        if (!fileSystem->rename(datafile, buf))
        {
          Serial.print(MSG[ERR]);
          Serial.println("Couldn't rename " + datafile + " to " + buf);
        }
        else
        {
          Serial.print(MSG[INFO]);
          Serial.println("Renamed " + datafile + " to " + buf + " and write data into it.");
          datafile = buf;
        }
      }
      else /* Should never happend. */
        datafile = "data/" + tz.dateTime("YmdHi") + ".csv";
    }
  }

  /* --- Get sensor data if available and write it to file. --- */
  if (getSensorData()) {
    /* Write to file */
    cbuf[0] = ';';
    buf = tzOK ? tz.dateTime(timeformat) : "";
    buf += cbuf[0]; /* ';' */
    buf += (int)co2;
    buf += cbuf[0];
    buf += temperature;
    buf += cbuf[0];
    buf += humidity;
    buf += cbuf[0];
    buf += millis();
    Serial.print(F("[MEASURE] "));
    Serial.println(buf);
    if ((writeData) && (fsOK)) { /* Write to file */
      file = LittleFS.open(datafile, "a");
      if (!file) {
        Serial.print(MSG[ERR]);
        Serial.print(F("Failed to open file for appending : "));
        Serial.println(datafile);
      } else {
        if (!file.println(buf)) {
          Serial.print(MSG[ERR]);
          Serial.print(F("Failed to write data to file "));
          Serial.println(datafile);
        }
        file.close();
      }
    }
    /* --- MQTT --- */
    publishMQTT();
  }

  HTTPserver.handleClient();
  HTTPSserver.handleClient();
  MDNS.update();
  delay(500);
}
