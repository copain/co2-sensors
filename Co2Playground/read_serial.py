#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import serial
from datetime import datetime

ser = serial.Serial('/dev/ttyACM0', '115200')
ser.write('\n'.encode('ascii'))  # convert to ASCII before sending it
while True:
  buf = ser.readline().decode('utf-8')
  if buf[0] == '#':
    print("Date;%s" % buf[1:-1])
  else:  
    print("%s;%s" %(datetime.now().isoformat(timespec='seconds'), buf[:-1]))
