# Co2-CircuitPlayground

## Installation

Le montage nécessite :

- un circuit [Adafuit Playground Classic](https://www.adafruit.com/product/3000)
- un capteur de CI2 de type [SCD30](https://www.sensirion.com/scd30).

La réalisation ne pose aucune difficulté ; il suffit d'alimenter le SCD30
sur une broche 3,3V de la carte Adafruit, et de relier entre elles les broches
SDA et SCL du circuit et du capteur.

Le programme utilise la plateforme de développement Arduino, avec les librairies
"Adafruit Circuit Playground" et "SparkFun_SCD30_Arduino_Library".
Un petit programme python utilisant la librairie serial est proposé pour
lire les données sur une connexion USB et les sauvegarder dans un fichier.

## Utilisation

Mis sous tension le circuit s'initialise en quelques secondes. La mesure de Co2
correspond à 200ppm par LED (du ruban) allumée. Si la LED rouge située
à côté du connecteur USB est allumée, alors il faut ajouter 100ppm.

![Photo of the Co2-Playground](images/Co2-Playground.jpg)

Sur la photo ci-dessus, le taux de Co2 est situé entre
1300 et 1400ppm : 6 LED du ruban (3 vertes, 1 jaune et 2 rouges) +
la LED rouge à côté de l'USB donnent 6×200 + 100 = 1300ppm.

Le bouton situé à droite lorsque le connecteur USB est devant permet
de désactiver ou réactiver le signal sonore (par un appui de quelques
secondes).
