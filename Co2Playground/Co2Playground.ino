/* Co2 sensor for Circuit Playground */
#include <Adafruit_CircuitPlayground.h>
#include <Wire.h>
#include <SPI.h>
#include "SparkFun_SCD30_Arduino_Library.h"


#define SERIAL_SPEED 115200
#define NB_LED     0x0A
#define RESOLUTION 200 // 200ppm per LED
#define LOOP_DELAY 120 /* Sleep time between 2 tests of available data, button pressed, ...*/
#define LONG_PRESS 15 /* LONG_PRESS * LOOP_DELAY ms is duration to switch sound ON/OFF  */
#define TEMPERATURE_DISPLAY 4000 /* ms. Can disrupt measuring delays. */

/************** LED COLORS **************/
/*  LOW  */
#define LOW_RED   0x00
#define LOW_GREEN 0xBB
#define LOW_BLUE  0x00

/*  MEDIUM  */
#define MID_RED   0x60
#define MID_GREEN 0x60
#define MID_BLUE  0x00

/*  HIGH  */
#define HI_RED    0xBB
#define HI_GREEN  0x00
#define HI_BLUE   0x00


/************** CO2 Sensor config **************/
#define MEDIUM_VALUE  800 // CO2 ppm
#define HIGH_VALUE    900 // CO2 ppm
#define ALERT_VALUE  1000 // CO2 ppm
#define MEASUREMENT_INTERVAL 29 /* seconds ; 30 gives 1 measure evey 31,6s */
#define ALTITUDE 404 // Les Cézeaux, Aubière, 63

SCD30 airSensor;
bool sound = true;

/**** Displays the CO2 measurement value with LEDs ****/
void co2_led(int v) {
  uint8_t i=0;
  uint8_t led = (uint8_t) (v / RESOLUTION);
  for(;i<led;i++) {
    if (((i+1) * RESOLUTION) >= ALERT_VALUE)
      CircuitPlayground.strip.setPixelColor(i, HI_RED, HI_GREEN, HI_BLUE);
    else if (((i+1) * RESOLUTION) >= MEDIUM_VALUE)
      CircuitPlayground.strip.setPixelColor(i, MID_RED, MID_GREEN, MID_BLUE);
    else
      CircuitPlayground.strip.setPixelColor(i, LOW_RED, LOW_GREEN, LOW_BLUE);
  }
  for (i = led; i<NB_LED; CircuitPlayground.strip.setPixelColor(i++, 0));
  CircuitPlayground.strip.show();

  CircuitPlayground.redLED(((v % RESOLUTION) > (RESOLUTION>>1))?HIGH:LOW);
}


/**** Displays the temperature measurement value with LEDs ****/
void temp_led(float v) {
  uint8_t i;
  uint8_t n,m;

  CircuitPlayground.redLED(LOW);
  if (v<0.0) {
    n = (uint8_t) (-v/10.0);
    for (i = 0; i < n; CircuitPlayground.strip.setPixelColor(i++, 0, 0, 0xFF));
    for (i = n; i < 5; CircuitPlayground.strip.setPixelColor(i++, 0));
    n = (((int) (-v)) % 10);
  } else {
    n = (uint8_t) (v/10.0);
    for (i = 0; i < n; CircuitPlayground.strip.setPixelColor(i++, 0xFF, 0, 0));
    for (i = n; i < 5; CircuitPlayground.strip.setPixelColor(i++, 0));
    n = ((int) v) % 10;
  }
  m = n>>1;
  n = n % 2;
  for (i = 0; i < m; CircuitPlayground.strip.setPixelColor(10-(++i), 0, 0xFF, 0));
  if (n) CircuitPlayground.strip.setPixelColor(10-(++i), 0x80, 0x80, 0);
  for (; i < 5; CircuitPlayground.strip.setPixelColor(10-(++i), 0));
  CircuitPlayground.strip.show();
}


/**** setup() ****/
void setup()
{
  uint8_t i;
  Serial.begin(SERIAL_SPEED);
  CircuitPlayground.begin();
  Wire.begin();
  delay(100);
  for (int j = ((int) NB_LED * RESOLUTION); j >= 0; j -= (RESOLUTION>>1)) {
    co2_led(j);
    delay(100);
  }
  for (i=0x00; i<NB_LED; CircuitPlayground.strip.setPixelColor(i++, 0));
  CircuitPlayground.strip.show();


  while (!airSensor.begin())
  {
    Serial.println("Air sensor not detected. Please check wiring. Waiting 10s...");
    for (i = 0; i < 20; delay(500))
      CircuitPlayground.redLED(i++&B00000001?HIGH:LOW);
  }
  airSensor.setMeasurementInterval(MEASUREMENT_INTERVAL);
  airSensor.setAltitudeCompensation(ALTITUDE);
  delay(200);
}

/**** loop() ****/
uint8_t long_b_pressed = 0;
float co2 = 0.0, temp = 0.0, humidity = 0.0;
int light = 0, temp2 = 0, snd_val = 0 ;
void loop() {
  uint8_t i;

  if (airSensor.dataAvailable()) {
    /* --- Get sensor values --- */
    co2 = airSensor.getCO2();
    temp = airSensor.getTemperature();
    humidity = airSensor.getHumidity();
    light = CircuitPlayground.lightSensor();

    /* --- If data is available in serial, then we assume a device has just been connected --- */
    if(Serial.available()>0) {
      while (Serial.available()>0) Serial.read();
      Serial.println("#CO2(ppm);Temp(C);Humidity(%);Light(int);Uptime(ms)");
    }

    /* --- Write data --- */
    Serial.print(co2);
    Serial.print(';');
    Serial.print(temp);
    Serial.print(';');
    Serial.print(humidity);
    Serial.print(';');
    Serial.print(light);
    Serial.print(';');
    Serial.print(millis());
    Serial.println();

    co2_led((int) co2);

    if (sound) {
      if (co2 > ALERT_VALUE) {
        CircuitPlayground.speaker.enable(true);
        for (uint8_t j = 0; j++<3; delay(200))
          for (i = 0; i++<5; delay(25))
            CircuitPlayground.playTone(1784, 25);
      } else if (co2 > HIGH_VALUE) {
        CircuitPlayground.speaker.enable(true);
        for (uint8_t j = 0; j++<3; delay(200))
          for (i = 0; i++<2; delay(25))
            CircuitPlayground.playTone(1784, 25);
      } else if (co2 > MEDIUM_VALUE) {
        CircuitPlayground.speaker.enable(true);
        for (i = 0; i++<2; delay(50))
          CircuitPlayground.playTone(1784, 50);
      }
    }
    CircuitPlayground.speaker.enable(false);
  }

  if (CircuitPlayground.leftButton()) {
    if (++long_b_pressed == LONG_PRESS) {
      sound = !sound;
      CircuitPlayground.speaker.enable(true);
      if (sound)
        for (i = 0; i++<2; delay(50))
          CircuitPlayground.playTone(882, 50);
      else
        CircuitPlayground.playTone(882, 1000);
      CircuitPlayground.speaker.enable(false);
    }
  } else long_b_pressed = 0;

  if (CircuitPlayground.rightButton()) {
    temp_led((int) temp);
    delay(TEMPERATURE_DISPLAY);
    co2_led((int) co2);
  }

  delay(LOOP_DELAY);
}
