# CO2 Sensors

Réalisation de deux types de systèmes de mesure de CO2,
basés sur un capteur [SCD30](https://www.sensirion.com/scd30).

## CO2-Playground
Système permettant une lecture visuelle du taux de CO2
et disposant d'une alerte sonore lorsque le taux dépasse
certains seuils. Il peut être utilisé de façon autonome
en le connectant à une source d'alimentation, mais peut
aussi être connecté à un ordinateur pour une lecture
série des valeurs mesurées.

Ce système est basé sur un nœud de type
[Adafuit Playground Classic](https://www.adafruit.com/product/3000).

## CO2-Huzzah
Il s'agit d'un système prévu pour une communication Wifi.

Basé sur un nœud de type
[Adafruit Feather HUZZAH with ESP8266](https://www.adafruit.com/product/2821),
il est à la fois client d'un réseau Wifi et point d'accès.
Sur ces deux interfaces, il permet une connexion HTTP ou HTTPS
pour lire les données mesurées. Il permet le stockage de ces
données sur une mémoire flash de 2Mo.
